package org.jcodec.common;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;

import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.BitmapUtil;
import org.jcodec.scale.ColorUtil;
import org.jcodec.scale.Transform;

/**
 * This class is part of JCodec ( www.jcodec.org ) This software is distributed
 * under FreeBSD License
 * 
 * @author The JCodec project
 * 
 */
public class HarmonyUtil {

    private static HarmonyUtil inst;
    private BitmapUtil bitmapUtil;

    public HarmonyUtil(BitmapUtil bitmapUtil) {
        this.bitmapUtil = bitmapUtil;
    }

    private static HarmonyUtil inst() {
        if (inst == null) {
            inst = new HarmonyUtil(new BitmapUtil());
        }
        return inst;
    }

    public static PixelMap toBitmap(Picture pic) {
        return inst().toBitmapImpl(pic);
    }

    public static void toBitmap(Picture pic, PixelMap out) {
        inst().toBitmapImpl(pic, out);
    }

    public static Picture fromBitmap(PixelMap bitmap, ColorSpace colorSpace) {
        return inst().fromBitmapImpl(bitmap, colorSpace);
    }

    public static Picture fromBitmap(PixelMap bitmap, VideoEncoder encoder) {
        return inst().fromBitmapImpl(bitmap, encoder);
    }

    public static void fromBitmap(PixelMap bitmap, Picture out) {
        inst().fromBitmapImpl(bitmap, out);
    }

    public PixelMap toBitmapImpl(Picture pic) {
        if (pic == null)
            return null;

        Transform transform = ColorUtil.getTransform(pic.getColor(), ColorSpace.RGB);
        Picture rgb = Picture.createCropped(pic.getWidth(), pic.getHeight(), ColorSpace.RGB, pic.getCrop());
        transform.transform(pic, rgb);
        return bitmapUtil.toBitmapImpl(rgb);
    }

    public void toBitmapImpl(Picture pic, PixelMap out) {
        if (pic == null)
            throw new IllegalArgumentException("Input pic is null");
        if (out == null)
            throw new IllegalArgumentException("Out bitmap is null");

        Transform transform = ColorUtil.getTransform(pic.getColor(), ColorSpace.RGB);
        Picture rgb = Picture.createCropped(pic.getWidth(), pic.getHeight(), ColorSpace.RGB, pic.getCrop());
        transform.transform(pic, rgb);
        bitmapUtil.toBitmapImpl(rgb, out);
    }

    public Picture fromBitmapImpl(PixelMap bitmap, ColorSpace colorSpace) {
        if (bitmap == null)
            return null;
        Picture out = Picture.create(bitmap.getImageInfo().size.width, bitmap.getImageInfo().size.height, colorSpace);
        fromBitmapImpl(bitmap, out);
        return out;
    }

    public Picture fromBitmapImpl(PixelMap bitmap, VideoEncoder encoder) {
        if (bitmap == null)
            return null;

        ColorSpace selectedColorSpace = null;
        for (ColorSpace colorSpace : encoder.getSupportedColorSpaces()) {
            if (ColorUtil.getTransform(ColorSpace.RGB, colorSpace) != null) {
                selectedColorSpace = colorSpace;
                break;
            }
        }
        if (selectedColorSpace == null) {
            throw new RuntimeException("Could not find a transform to convert to a codec-supported colorspace.");
        }

        Picture out = Picture.create(bitmap.getImageInfo().size.width, bitmap.getImageInfo().size.height, selectedColorSpace);
        fromBitmapImpl(bitmap, out);
        return out;
    }
    

    public void fromBitmapImpl(PixelMap bitmap, Picture out) {
        if (bitmap == null)
            throw new IllegalArgumentException("Input pic is null");
        if (out == null)
            throw new IllegalArgumentException("Out bitmap is null");

        if (bitmap.getImageInfo().pixelFormat.ARGB_8888 != PixelFormat.ARGB_8888) {
            throw new RuntimeException("Unsupported bitmap config ");
        }
        Picture rgb = bitmapUtil.fromBitmapImpl(bitmap);

        Transform transform = ColorUtil.getTransform(ColorSpace.RGB, out.getColor());
        transform.transform(rgb, out);
    }
}