package org.jcodec.scale;

import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.media.image.common.Rect;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.ScaleMode;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;

import java.nio.IntBuffer;
import ohos.media.image.PixelMap;

/**
 * This class is part of JCodec ( www.jcodec.org ) This software is distributed
 * under FreeBSD License
 * 
 * @author The JCodec project
 * 
 */
public class BitmapUtil {
    private final static ThreadLocal<BitmapUtil> inst = new ThreadLocal<BitmapUtil>();
    private int[] buffer;

    private static BitmapUtil inst() {
        BitmapUtil i = inst.get();
        if (i == null) {
            i = new BitmapUtil();
            inst.set(i);
        }
        return i;
    }

    public static Picture fromBitmap(PixelMap src) {
        return inst().fromBitmapImpl(src);
    }

    public static void fromBitmap(PixelMap src, Picture dst) {
        inst().fromBitmapImpl(src, dst);
    }

    public static PixelMap toBitmap(Picture pic) {
        return inst().toBitmapImpl(pic);
    }

    public static void toBitmap(Picture src, PixelMap dst) {
        inst().toBitmapImpl(src, dst);
    }

    public Picture fromBitmapImpl(PixelMap src) {
        if (src == null)
            return null;
        Picture dst = Picture.create(src.getImageInfo().size.width, src.getImageInfo().size.height, ColorSpace.RGB);
        fromBitmapImpl(src, dst);
        return dst;
    }

    public void fromBitmapImpl(PixelMap src, Picture dst) {
        byte[] dstData = dst.getPlaneData(0);
        int[] packed = getBuffer(src.getImageInfo().size.width, src.getImageInfo().size.height);
        src.readPixels(packed, 0, src.getImageInfo().size.width, new Rect(0, 0, src.getImageInfo().size.width, src.getImageInfo().size.height));

        for (int i = 0, srcOff = 0, dstOff = 0; i < src.getImageInfo().size.height; i++) {
            for (int j = 0; j < src.getImageInfo().size.width; j++, srcOff++, dstOff += 3) {
                int rgb = packed[srcOff];
                dstData[dstOff] = (byte) (((rgb >> 16) & 0xff) - 128);
                dstData[dstOff + 1] = (byte) (((rgb >> 8) & 0xff) - 128);
                dstData[dstOff + 2] = (byte) ((rgb & 0xff) - 128);
            }
        }
    }

    private PixelMap.InitializationOptions getdefaultinitOptions(int width, int height,  PixelFormat config) {
        PixelMap.InitializationOptions opts = new PixelMap.InitializationOptions();
        opts.alphaType = AlphaType.OPAQUE;
        opts.editable = true;
        opts.pixelFormat = config;
        opts.releaseSource = false;
        opts.scaleMode = ScaleMode.FIT_TARGET_SIZE;
        opts.size = new Size(width, height);
        opts.useSourceIfMatch = true;

        return opts;
    }

    public PixelMap toBitmapImpl(Picture pic) {
        if (pic == null)
            return null;

        PixelMap.InitializationOptions opts = getdefaultinitOptions(pic.getCroppedWidth(), pic.getCroppedHeight(), PixelFormat.ARGB_8888);
        PixelMap dst = PixelMap.create(opts);
        toBitmapImpl(pic, dst);
        return dst;
    }

    public void toBitmapImpl(Picture src, PixelMap dst) {
        byte[] srcData = src.getPlaneData(0);
        int[] packed = getBuffer(src.getWidth(), src.getHeight());

        for (int i = 0, dstOff = 0, srcOff = 0; i < src.getCroppedHeight(); i++) {
            for (int j = 0; j < src.getCroppedWidth(); j++, dstOff++, srcOff += 3) {
                packed[dstOff] = (255 << 24) | ((srcData[srcOff + 2] + 128) << 16) | ((srcData[srcOff + 1] + 128) << 8)
                        | (srcData[srcOff] + 128);
            }
            srcOff += src.getWidth() - src.getCroppedWidth();
        }
        dst.writePixels(IntBuffer.wrap(packed));
    }

    private int[] getBuffer(int width, int height) {
        if (buffer == null || buffer.length != width * height) {
            buffer = new int[width * height];
        }
        return buffer;
    }
}