package org.jcodec.api.harmony;

import ohos.media.image.PixelMap;

import org.jcodec.api.SequenceEncoder;
import org.jcodec.common.Codec;
import org.jcodec.common.Format;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.io.SeekableByteChannel;
import org.jcodec.common.model.Rational;
import org.jcodec.scale.BitmapUtil;

import java.io.File;
import java.io.IOException;

/**
 * This class is part of JCodec ( www.jcodec.org ) This software is distributed
 * under FreeBSD License
 * 
 * @author The JCodec project
 * 
 */
public class HarmonySequenceEncoder extends SequenceEncoder {

    public static HarmonySequenceEncoder createSequenceEncoder(File out, int fps) throws IOException {
        return new HarmonySequenceEncoder(NIOUtils.writableChannel(out), Rational.R(fps, 1));
    }

    public static HarmonySequenceEncoder create25Fps(File out) throws IOException {
        return new HarmonySequenceEncoder(NIOUtils.writableChannel(out), Rational.R(25, 1));
    }

    public static HarmonySequenceEncoder create30Fps(File out) throws IOException {
        return new HarmonySequenceEncoder(NIOUtils.writableChannel(out), Rational.R(30, 1));
    }

    public static HarmonySequenceEncoder create2997Fps(File out) throws IOException {
        return new HarmonySequenceEncoder(NIOUtils.writableChannel(out), Rational.R(30000, 1001));
    }

    public static HarmonySequenceEncoder create24Fps(File out) throws IOException {
        return new HarmonySequenceEncoder(NIOUtils.writableChannel(out), Rational.R(24, 1));
    }

    public HarmonySequenceEncoder(SeekableByteChannel ch, Rational fps) throws IOException {
        super(ch, fps, Format.MOV, Codec.H264, null);
    }

    public void encodeImage(PixelMap bi) throws IOException {
        encodeNativeFrame(BitmapUtil.fromBitmap(bi));
    }
}