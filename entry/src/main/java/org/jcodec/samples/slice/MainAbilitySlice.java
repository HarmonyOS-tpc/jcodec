/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jcodec.samples.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.media.image.PixelMap;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Picture;
import org.jcodec.common.HarmonyUtil;
import org.jcodec.common.LogUtil;

import java.io.File;
import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    private static String TAG = MainAbilitySlice.class.getSimpleName();

    private static final int ID_BUTTON = 200;

    private Button button;
    private Image imageView;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        DirectionalLayout directionLayout = new DirectionalLayout(this);
        // Step 2 Set the layout size.
        directionLayout.setWidth(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        directionLayout.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        // Step 3 Set the layout attributes and ID (set the ID as required).
        directionLayout.setOrientation(Component.VERTICAL);

        // Step 4.1 Add layout attributes for the control.
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);

        // Step 4.2 Add Text to the layout.
        // Add a button in the similar way.
        button = new Button(this);
        button.setLayoutConfig(layoutConfig);
        button.setText("Get Single Frame");
        button.setTextSize(60);
        button.setId(ID_BUTTON);
        ShapeElement background = new ShapeElement();
        background.setRgbColor(RgbColor.fromArgbInt(Color.GREEN.getValue()));
        button.setBackground(background);
        button.setPadding(10, 10, 10, 10);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component Component) {  // Add a listener for the click event to the contro
                LogUtil.info(TAG, "onClick() of Button1");
                LogUtil.info(TAG, "Test tagging for jCodec");

                try {
                    getFrame();

                } catch (IOException e) {
                    LogUtil.info(TAG, "IOException");
                    e.printStackTrace();
                }
                catch (JCodecException e) {
                    LogUtil.info(TAG, "JCodecException");
                    e.printStackTrace();
                }
            }
        });

        directionLayout.addComponent(button);

        imageView = new Image(this);
        imageView.setLayoutConfig(layoutConfig);
        imageView.setPadding(10, 10, 10, 10);
        directionLayout.addComponent(imageView);

        super.setUIContent(directionLayout);
    }


    private void getFrame() throws IOException, JCodecException {
        LogUtil.info(TAG, "getFrame");

        int frameNumber = 322;
        Picture picture = FrameGrab.getFrameFromFile(new File("/data/user/0/org.jcodec.samples/cache/jcodec_cache/sample.mp4"), frameNumber);

        //for openharmony (jcodec-openharmony)
        PixelMap bitmap = HarmonyUtil.toBitmap(picture);

        if(null != bitmap) {
            LogUtil.info(TAG, "bitmap is not null");
            imageView.setPixelMap( bitmap);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}